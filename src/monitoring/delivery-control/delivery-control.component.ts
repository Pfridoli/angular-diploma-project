import {Component, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../../services/data.service';
import {forkJoin, Subscription} from 'rxjs';
import {FoodHandlingPlace, User} from '../../../../user-data-types';
import {FoodDevice} from '../../../models/food-device.model';
import {Deliveries, DeliveryForm} from '../../../models/deliveries.model';
import {MonitoringService} from '../../../services/monitoring.service';
import {ToastService} from '@app/services/toast-service.service';
import {Router} from '@angular/router';
import * as _ from 'lodash';
import {MONITORING_TOAST} from '../../../constants/monitoring.constants';
import {map} from 'rxjs/operators';
import {FileService} from '../../../../services/file.service';
import {PictureAttachments} from '../../../models/attachments.model';

@Component({
    selector: 'app-delivery-control',
    templateUrl: './delivery-control.component.html',
    styleUrls: ['./delivery-control.component.scss']
})
export class DeliveryControlComponent implements OnInit, OnDestroy {
    httpError: boolean;
    deliveryForm: FormGroup;
    private deliveriesList: Deliveries[];
    private originalForm: DeliveryForm[];
    private langSubscription: Subscription;
    private placeSubscription: Subscription;
    private currentLang: string;
    loading: boolean;
    private currentUser: User;
    private newAttachmentList: PictureAttachments[];
    private missingStatusPicture: boolean;
    private missingDeliveryPicture: DeliveryForm;
    private currentDates: string[];
    delBtnState: string;
    headers = [
        {name: 'time', size: '15%'},
        {name: 'Supplier', size: '16%'},
        {name: 'Product', size: '16%'},
        {name: 'Product temp', size: '9%'},
        {name: 'Defects', size: '19%'},
        {name: 'What I did', size: '19%'},
        {name: '', size: '6%'}
    ];

    defects: { id: number; name: string }[] = [
        {id: 1, name: 'Expiration date exceeded'},
        {id: 2, name: 'Product is not quality'},
        {id: 3, name: 'Package was broken'},
        {id: 4, name: 'Temperature was not correct'}
    ];

    preventions: { id: number; name: string }[] = [
        {id: 1, name: 'Employees were trained'},
        {id: 2, name: 'Guidance was modified'},
        {id: 3, name: 'Returned goods'},
        {id: 4, name: 'Food was reprocessed'},
        {id: 5, name: 'Food was rejected'},
        {id: 6, name: 'Supplier was notified'}
    ];

    @Input()
    deviceId: number;

    @HostListener('document:click', ['$event'])
    clickedOutside() {
        this.closeAllDropdowns();
    }

    constructor(
        public dataService: DataService,
        private fb: FormBuilder,
        private monitoringService: MonitoringService,
        private toastService: ToastService,
        private router: Router,
        private fileService: FileService
    ) {
        this.deliveriesList = [];
        this.newAttachmentList = [];
    }

    ngOnInit(): void {
        this.initForm();
        this.onLanguageChange();
        this.initData();
    }

    getDelivery(index: number): FormGroup {
        return this.deliveryForm.get('deliveries').get([index]) as FormGroup;
    }

    addDelivery(): void {
        if (
            this.dataService.checkPermission() &&
            !this.dataService.isReadOnly() &&
            !this.dataService.isReadonlyMeasurement()
        ) {
            (<FormArray>this.deliveryForm.get('deliveries')).insert(0, this.addOneDelivery(null));
            this.originalForm = JSON.parse(JSON.stringify(this.deliveryForm.value.deliveries));
        } else {
            this.toastService.showToast('error', 'No permission to do that');
        }
    }

    ngOnDestroy(): void {
        this.onSave().then(() => {
        }, () => {
        });
        if (this.langSubscription) {
            this.langSubscription.unsubscribe();
        }
        if (this.placeSubscription) {
            this.placeSubscription.unsubscribe();
        }
    }

    private onLanguageChange(): void {
        this.langSubscription = this.dataService.language$.subscribe((lang: string) => {
            if (!lang || lang === 'ru') {
                if (lang) {
                    this.dataService.setLanguage('en');
                }
                this.currentLang = this.dataService.getLanguage();
            } else {
                this.currentLang = lang;
            }
        });
    }

    private initForm() {
        this.deliveryForm = this.fb.group({
            deliveries: this.initDeliveries()
        });
        this.originalForm = JSON.parse(JSON.stringify(this.deliveryForm.value.deliveries));
    }

    private initDeliveries(): FormArray {
        if (this.deliveriesList && this.deliveriesList.length) {
            const controls = [];
            _.forEach(this.deliveriesList, (delivery: Deliveries) => {
                controls.push(this.addOneDelivery(delivery));
            });
            return this.fb.array(controls, Validators.required);
        } else {
            return this.fb.array([]);
        }
    }

    get deliveries(): FormArray {
        return this.deliveryForm.get('deliveries') as FormArray;
    }

    private addOneDelivery(delivery: Deliveries): FormGroup {
        let formattedDate: string;
        if (!delivery) {
            const currentDate: Date = new Date(Date.now());
            formattedDate = `${currentDate.getFullYear()}-${('0' + (currentDate.getMonth() + 1)).slice(-2)}-${(
                '0' + currentDate.getDate()
            ).slice(-2)} ${('0' + currentDate.getHours()).slice(-2)}:${('0' + currentDate.getMinutes()).slice(-2)}:${(
                '0' + currentDate.getSeconds()
            ).slice(-2)}`;
        }
        return this.fb.group({
            time: [delivery ? delivery.time : formattedDate, [Validators.required]],
            measurer: [delivery ? delivery.measurer : null, Validators.required],
            data: this.fb.group({
                supplier: [delivery && delivery.data.supplier ? delivery.data.supplier : null, Validators.required],
                product: [delivery && delivery.data.product ? delivery.data.product : null, Validators.required],
                quality: [delivery && delivery.data.quality ? delivery.data.quality : null, Validators.required]
            }),
            value: [delivery && delivery.value ? delivery.value : null, Validators.required],

            correcting_text: [delivery && delivery.correcting_text ? delivery.correcting_text : null, Validators.required],
            attachments: [delivery && delivery.attachments ? delivery.attachments : null],
            id: [delivery ? delivery.id : null]
        });
    }

    private createDeliveries(deliveries: DeliveryForm[]): Promise<void> {
        return new Promise((resolve, reject) => {
            if (deliveries.length) {
                this.monitoringService.createDeliveries(deliveries, this.deviceId).subscribe(
                    (res: DeliveryForm[]) => {
                        if (this.missingStatusPicture) {
                            res.map(delivery => {
                                this.missingDeliveryPicture = delivery;
                            });
                        }
                        resolve();
                    },
                    () => {
                        reject();
                    }
                );
            } else {
                resolve();
            }
        });
    }

    getDateValues(dates: string[]): void {
        this.currentDates = dates;
        this.getDeliveriesList(dates[0], dates[1]);
    }

    private updateDeliveries(deliveries: DeliveryForm[]): Promise<void> {
        return new Promise((resolve, reject) => {
            if (deliveries.length) {
                this.monitoringService.updateDeliveries(deliveries).subscribe(
                    () => {
                        resolve();
                    },
                    () => {
                        reject();
                    }
                );
            } else {
                resolve();
            }
        });
    }

    checkUserUpdate(index: number): void {
        if (
            (this.deliveryForm.get('deliveries').value[index].data.supplier &&
                this.deliveryForm.get('deliveries').value[index].data.supplier !== this.originalForm[index].data.supplier) ||
            (this.deliveryForm.get('deliveries').value[index].data.product &&
                this.deliveryForm.get('deliveries').value[index].data.product !== this.originalForm[index].data.product) ||
            (this.deliveryForm.get('deliveries').value[index].value &&
                this.deliveryForm.get('deliveries').value[index].value !== this.originalForm[index].value) ||
            (this.deliveryForm.get('deliveries').value[index].data.quality &&
                this.deliveryForm.get('deliveries').value[index].data.quality !== this.originalForm[index].data.quality) ||
            (this.deliveryForm.get('deliveries').value[index].correcting_text &&
                this.deliveryForm.get('deliveries').value[index].correcting_text !== this.originalForm[index].correcting_text)
        ) {
            (<FormArray>this.deliveryForm.get('deliveries')).at(index).patchValue({measurer: this.currentUser.id});
        } else {
            if (
                this.deliveryForm.get('deliveries').value[index].id &&
                (this.deliveryForm.get('deliveries').value[index].data.supplier &&
                    this.deliveryForm.get('deliveries').value[index].data.supplier === this.originalForm[index].data.supplier) &&
                (this.deliveryForm.get('deliveries').value[index].data.product &&
                    this.deliveryForm.get('deliveries').value[index].data.product === this.originalForm[index].data.product) &&
                (this.deliveryForm.get('deliveries').value[index].value &&
                    this.deliveryForm.get('deliveries').value[index].value === this.originalForm[index].value) &&
                (this.deliveryForm.get('deliveries').value[index].data.quality &&
                    this.deliveryForm.get('deliveries').value[index].data.quality === this.originalForm[index].data.quality) &&
                (this.deliveryForm.get('deliveries').value[index].correcting_text &&
                    this.deliveryForm.get('deliveries').value[index].correcting_text === this.originalForm[index].correcting_text)
            ) {
                /*const data: any = _.find(this.deliveriesList, {
                  id: this.deliveryForm.get('deliveries').value[index].id
                });*/
                const data: any = this.deliveriesList.find(
                    (delivery: Deliveries) => delivery.id === Number(this.deliveryForm.get('deliveries').value[index].id)
                );
                if (!_.isEmpty(data)) {
                    (<FormArray>this.deliveryForm.get('deliveries')).at(index).patchValue({measurer: data.measurer_user.id});
                }
            } else {
                (<FormArray>this.deliveryForm.get('deliveries')).at(index).patchValue({measurer: null});
            }
        }
    }

    showPicture(attachmentId: number, deliveryIndex: number): void {
        if (attachmentId !== null) {
            this.monitoringService.getProductImage(attachmentId).subscribe(
                (res: any) => {
                    const fileURL = URL.createObjectURL(res);
                    const a = document.createElement('a');
                    let attachmentPicture: PictureAttachments = null;
                    a.href = fileURL;
                    this.deliveries
                        .at(deliveryIndex)
                        .get('attachments')
                        .value.forEach(attachment => {
                        if (attachmentId === attachment.id) {
                            attachmentPicture = attachment;
                        }
                    });
                    a.download = attachmentPicture.display_name;
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);
                },
                () => {
                    // this.toastService.showToast('error', 'Failed to display a picture');
                }
            );
        } else {
            this.toastService.showToast('error', 'No file(s) found');
        }
    }

    deletePicture(questionIndex: number, attachmentId: number): void {
        if (attachmentId !== null) {
            this.monitoringService.deleteProductImage(attachmentId).subscribe(
                () => {
                    this.deliveries
                        .at(questionIndex)
                        .get('attachments')
                        .value.forEach(attachment => {
                        if (attachmentId !== attachment.id) {
                            this.newAttachmentList.push(attachment);
                        }
                    });
                    this.deliveries
                        .at(questionIndex)
                        .get('attachments')
                        .setValue(this.newAttachmentList);
                    this.newAttachmentList = [];
                },
                () => {
                }
            );
        } else {
            this.toastService.showToast('error', 'error_removing_file');
        }
    }

    private getCurrentDate(): string {
        const currentDate: Date = new Date(Date.now());
        return `${currentDate.getFullYear()}-${('0' + (currentDate.getMonth() + 1)).slice(-2)}-${(
            '0' + currentDate.getDate()
        ).slice(-2)} ${('0' + currentDate.getHours()).slice(-2)}:${('0' + currentDate.getMinutes()).slice(-2)}:${(
            '0' + currentDate.getSeconds()
        ).slice(-2)}`;
    }

    checkImage(fileInput: any, deliveryId: number, deliveryIndex: number): void {
        if (fileInput.target.files && fileInput.target.files.length > 0) {
            const file = fileInput.target.files[0];

            if (
                file.type !== 'image/jpeg' &&
                file.type !== 'image/png' &&
                file.type !== 'image/jpg' &&
                file.type !== 'image/gif'
            ) {
                this.toastService.showToast('error', 'Only PNG, JPEG, JPG and GIF files can be added');
                return;
            }

            if (file.size > 5240000) {
                this.toastService.showToast('error', 'file_more_than_5mb');
                return;
            }

            const fileReader = new FileReader();

            fileReader.addEventListener('load', () => {

                this.addPicture(
                    file.name,
                    this.fileService.getBase64FromDataURL(fileReader.result),
                    deliveryId,
                    deliveryIndex
                );
            });
            fileReader.readAsDataURL(file);
            fileInput.target.value = null;
        }
    }

    private addPicture(importPictureName: string, importPictureData: string, deliveryId: number, deliveryIndex: number) {
        if (importPictureName !== null && importPictureData !== null) {
            const picObject = {
                certificate_file: importPictureData,
                certificate_file_name: importPictureName
            };

            if (!deliveryId) {
                (<FormArray>this.deliveryForm.get('deliveries'))
                    .at(deliveryIndex)
                    .patchValue({measurer: this.currentUser.id});
                this.missingStatusPicture = true;
                this.onSave().then(
                    () => {
                        deliveryId = this.missingDeliveryPicture.id;

                        this.monitoringService.uploadImageToProduct(deliveryId, picObject).subscribe(
                            response => {
                                this.deliveries
                                    .at(deliveryIndex)
                                    .get('attachments')
                                    .setValue(response.attachments);
                            },
                            () => {
                            }
                        );

                        this.getDateValues(this.currentDates);
                        this.missingStatusPicture = false;
                        this.missingDeliveryPicture = null;
                    },
                    () => {
                        (<FormArray>this.deliveryForm.get('deliveries')).at(deliveryIndex).patchValue({measurer: null});
                        this.missingDeliveryPicture = null;
                    }
                );
            } else {
                this.monitoringService.uploadImageToProduct(deliveryId, picObject).subscribe(
                    response => {
                        this.deliveries
                            .at(deliveryIndex)
                            .get('attachments')
                            .setValue(response.attachments);
                    },
                    () => {
                    }
                );
            }
        } else {
            this.toastService.showToast('error', 'No file(s) found');
        }
    }

    private getDeliveriesList(startDate: string, endDate: string): void {
        this.loading = true;
        this.monitoringService
            .getDeliveriesForFoodDevice(this.deviceId, startDate, endDate)
            .pipe(
                map(data => {
                    return data.map(item => {
                        item['correcting_text'] = item.correcting_text
                            ? (<string>item.correcting_text).split(',')
                            : item.correcting_text;
                        if (item['data'] !== null) {
                            item['data'].quality = item.data.quality ? (<string>item.data.quality).split(',') : item.data.quality;
                        }
                        return item;
                    });
                }),
                map(data => _.orderBy(data, ['time'], ['desc']))
            )
            .subscribe(
                (deliveriesList: Deliveries[]) => {
                    this.deliveriesList = deliveriesList;
                    this.initForm();
                    this.loading = false;
                    this.httpError = false;
                },
                () => {
                    this.deliveriesList = [];
                    this.initForm();
                    this.httpError = true;
                    this.loading = false;
                    this.toastService.show(MONITORING_TOAST.somethingWentWrong);
                }
            );
    }

    setDropdownStatus(delivery: Deliveries, event: Event): void {
        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }
        this.deliveries.controls.forEach(deliveries => {
            if (deliveries.value.id !== delivery.id) {
                deliveries.value.isOpen = false;
            }
        });
        delivery.isOpen = !delivery.isOpen;

    }

    private closeAllDropdowns(): void {
        this.deliveries.controls.forEach(deliveries => {
            if (deliveries) {
                deliveries.value.isOpen = false;
            }
        });
    }

    private onSave(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (
                this.deliveryForm.value.deliveries &&
                this.deliveryForm.value.deliveries.length &&
                !_.isEqual(this.deliveryForm.value.deliveries, this.originalForm)
            ) {
                this.loading = true;
                const newValues = _.partition(this.deliveryForm.value.deliveries, (delivery: DeliveryForm) => {
                    if (delivery.id) {
                        return true;
                    }
                });
                newValues[0] = _.filter(newValues[0], (delivery: DeliveryForm) => {
                    // const data = _.find(this.originalForm, { id: delivery.id });
                    const data = this.originalForm.find(
                        (deliveryForm: DeliveryForm) => deliveryForm.id === Number(this.deviceId)
                    );
                    if (!_.isEmpty(data) && !_.isEqual(data, delivery)) {
                        delivery.data.quality = delivery.data.quality ? delivery.data.quality.toString() : delivery.data.quality;
                        delivery.correcting_text = delivery.correcting_text
                            ? delivery.correcting_text.toString()
                            : delivery.correcting_text;
                        return true;
                    }
                });

                newValues[1] = _.filter(newValues[1], (delivery: DeliveryForm) => {
                    if (
                        delivery.data.supplier ||
                        delivery.data.product ||
                        delivery.value ||
                        delivery.data.quality ||
                        delivery.correcting_text ||
                        delivery.measurer
                    ) {
                        delivery.data.quality = delivery.data.quality ? delivery.data.quality.toString() : delivery.data.quality;
                        delivery.correcting_text = delivery.correcting_text
                            ? delivery.correcting_text.toString()
                            : delivery.correcting_text;
                        return true;
                    }
                });

                this.updateDeliveries(newValues[0]).then(
                    () => {
                    },
                    () => {
                        this.loading = false;
                        // this.toastService.show(MONITORING_TOAST.deviceSaveError);
                        reject();
                    }
                );

                this.createDeliveries(newValues[1]).then(
                    () => {
                        this.loading = false;
                        resolve();
                    },
                    () => {
                        this.loading = false;
                        // this.toastService.show(MONITORING_TOAST.deviceSaveError);
                        reject();
                    }
                );
            } else {
                this.loading = false;
                reject();
            }
        });
    }

    deleteDelivery(event: Event, index: number): void {
        if (
            this.dataService.checkPermission() &&
            !this.dataService.isReadOnly() &&
            !this.dataService.isReadonlyMeasurement()
        ) {
            if (this.delBtnState === 'confirm') {
                this.delBtnState = 'done';
                this.monitoringService.deleteMeasurement(this.deliveries.value[index].id).subscribe(() => {
                    (<FormArray>this.deliveryForm.get('deliveries')).removeAt(index);
                });
            } else {
                this.delBtnState = 'confirm';
                event.stopPropagation();
            }
        } else {
            this.toastService.showToast('error', 'No permission to do that');
        }
    }

    private initData(): void {
        if (this.deviceId) {
            this.loading = true;
            this.placeSubscription = this.dataService.selectedPlace$.subscribe((selectedPlace: FoodHandlingPlace) => {
                if (selectedPlace) {
                    forkJoin([this.dataService.getProfile()]).subscribe(
                        (data: any) => {
                            this.currentUser = data[0];
                            this.loading = false;
                        },
                        () => {
                            this.loading = false;
                            this.toastService.show(MONITORING_TOAST.somethingWentWrong);
                        }
                    );
                }
            });
        }
    }
}
