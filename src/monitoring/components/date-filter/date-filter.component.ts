import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import * as _ from 'lodash';

import { DATE_FILTER } from '../../../constants/monitoring.constants';

@Component({
  selector: 'app-date-filter',
  templateUrl: '../../../../../../../../angular-diploma-project/src/components/date-filter/date-filter.component.html',
  styleUrls: ['./date-filter.component.scss']
})
export class DateFilterComponent implements OnInit {
  selectedDates: Date[] = [];
  searchTerm: string;

  @Input() showGraph: boolean;
  @Input() showSearch = false;

  @Output() emitSelectedDates: EventEmitter<string[]> = new EventEmitter<string[]>();
  @Output() emitOpenGraphClick: EventEmitter<void> = new EventEmitter<void>();
  @Output() emitSearchProcess: EventEmitter<string> = new EventEmitter<string>();

  selectedFilter = DATE_FILTER.CURRENT_MONTH.value;
  DATE_FILTER = _.sortBy(DATE_FILTER, ['order']);

  constructor() {}

  ngOnInit() {
    this.getSelectedDates(this.selectedFilter);
  }

  onChange(): void {
    if (this.selectedDates && this.selectedDates[0] && this.selectedDates[1]) {
      const modifiedDates: string[] = [
        this.formatDate(new Date(this.selectedDates[0])),
        this.formatDate(new Date(this.selectedDates[1]))
      ];
      this.emitDates(modifiedDates);
    }
  }

  formatDate(date: Date): string {
    return `${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}-${('0' + date.getDate()).slice(-2)}`;
  }

  filterData(filterValue: string): void {
    this.selectedFilter = filterValue;
    this.getSelectedDates(filterValue);
  }

  getSelectedDates(filterValue: string): void {
    switch (filterValue) {
      case DATE_FILTER.TODAY.value:
        {
          const todayDate = new Date(Date.now());
          this.selectedDates = [todayDate, todayDate];
          const modifiedDateStart = `${todayDate.getFullYear()}-${('0' + (todayDate.getMonth() + 1)).slice(-2)}-${(
            '0' + todayDate.getDate()
          ).slice(-2)}`;
          const modifiedDateEnd = `${todayDate.getFullYear()}-${('0' + (todayDate.getMonth() + 1)).slice(-2)}-${(
            '0' + todayDate.getDate()
          ).slice(-2)}`;
          this.emitDates([modifiedDateStart, modifiedDateEnd]);
        }
        return;
      case DATE_FILTER.YESTERDAY.value:
        {
          const todayDate = new Date(Date.now());
          const yesterdayDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate() - 1);
          this.selectedDates = [yesterdayDate, yesterdayDate];
          const modifiedDateStart = `${todayDate.getFullYear()}-${('0' + (todayDate.getMonth() + 1)).slice(-2)}-${(
            '0' +
            (todayDate.getDate() - 1)
          ).slice(-2)}`;
          const modifiedDateEnd = `${todayDate.getFullYear()}-${('0' + (todayDate.getMonth() + 1)).slice(-2)}-${(
            '0' +
            (todayDate.getDate() - 1)
          ).slice(-2)}`;
          this.emitDates([modifiedDateStart, modifiedDateEnd]);
        }
        return;
      case DATE_FILTER.CURRENT_MONTH.value:
        {
          const date = new Date();
          const startDate = this.formatDate(new Date(date.getFullYear(), date.getMonth(), 1));
          const endDate = this.formatDate(new Date(date.getFullYear(), date.getMonth() + 1, 0));
          this.selectedDates = [
            new Date(date.getFullYear(), date.getMonth(), 1),
            new Date(date.getFullYear(), date.getMonth() + 1, 0)
          ];
          this.emitDates([startDate, endDate]);
        }
        return;
      case DATE_FILTER.LAST_MONTH.value:
        {
          const date = new Date();
          const startDate = this.formatDate(new Date(date.getFullYear(), date.getMonth() - 1, 1));
          const endDate = this.formatDate(new Date(date.getFullYear(), date.getMonth(), 0));
          this.selectedDates = [
            new Date(date.getFullYear(), date.getMonth() - 1, 1),
            new Date(date.getFullYear(), date.getMonth(), 0)
          ];
          this.emitDates([startDate, endDate]);
        }
        return;
    }
  }

  emitDates(dateValues: string[]): void {
    this.emitSelectedDates.emit(dateValues);
  }

  openGraphicalRepresentation(): void {
    this.emitOpenGraphClick.emit();
  }

  onSearchTermInput(): void {
    this.emitSearchProcess.emit(this.searchTerm);
  }
}
